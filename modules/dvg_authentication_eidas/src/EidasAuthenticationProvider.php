<?php

namespace Drupal\dvg_authentication_eidas;

use Drupal\dvg_authentication\DvgAuthenticationException;
use Drupal\dvg_authentication\SamlAuthenticationProviderBase;
use Drupal\dvg_authentication\SamlUser;

/**
 * Class EidasAuthenticationProvider.
 */
class EidasAuthenticationProvider extends SamlAuthenticationProviderBase {

  /**
   * Authentication levels.
   */
  const LEVEL_LOW = 'low';
  const LEVEL_SUBSTANTIAL = 'substantial';
  const LEVEL_HIGH = 'high';

  /**
   * List of supported eIDAS versions.
   *
   * @var array
   */
  protected static $versions = ['1.11'];

  /**
   * List of namespaces per version keyed by the eIDAS version.
   *
   * @var array
   */
  protected static $xmlNamespaces = [
    '1.11' => 'urn:etoegang:',
  ];

  /**
   * {@inheritdoc}
   *
   * @var array
   */
  protected $samlAttributeMapping = [
    'PersonIdentifier' => 'identifier',
    'FirstName' => 'first_name',
    'FamilyNameInfix' => 'infix',
    'FamilyName' => 'last_name',
    'DateOfBirth' => 'date_of_birth',
  ];

  /**
   * {@inheritdoc}
   */
  protected $isLogoutReturnToCallbackSupported = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return 'eidas';
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return t('eIDAS');
  }

  /**
   * {@inheritdoc}
   */
  public function getButtonDescription() {
    return t('Login with your own nationally issued electronic identity credentials.');
  }

  /**
   * {@inheritdoc}
   */
  public function getLevels() {
    return [
      static::LEVEL_LOW => t('Low'),
      static::LEVEL_SUBSTANTIAL => t('Substantial'),
      static::LEVEL_HIGH => t('High'),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @see https://afsprakenstelsel.etoegang.nl/display/as/Interface+specifications+DV-HM
   */
  protected function getMinimumAuthnContextClassRef($level) {
    switch ($level) {
      case static::LEVEL_LOW:
        return 'urn:etoegang:core:assurance-class:loa2';

      case static::LEVEL_SUBSTANTIAL:
        return 'urn:etoegang:core:assurance-class:loa3';

      case static::LEVEL_HIGH:
        return 'urn:etoegang:core:assurance-class:loa4';

      default:
        throw new DvgAuthenticationException('Invalid authentication level');

    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultConfiguration() {
    return [
      'show_confirmation_page' => FALSE,
      'version' => '1.11',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm() {
    $form = parent::buildConfigurationForm();
    $form['version'] = [
      '#type' => 'select',
      '#title' => t('eIDAS version'),
      '#options' => drupal_map_assoc(static::$versions),
      '#default_value' => $this->getConfig('version'),
      '#required' => TRUE,
      '#weight' => -2,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function parseSamlAttributes(array $saml_attributes) {
    $attributes = parent::parseSamlAttributes($saml_attributes);
    // Format the date_of_birth attribute.
    if (!empty($attributes['date_of_birth'])) {
      $date_of_birth = new \DateObject(strtotime($attributes['date_of_birth']));
      $attributes['date_of_birth'] = date_format_date($date_of_birth, 'custom', 'd-m-Y');
    }
    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getUser(\stdClass $account) {
    return new SamlUser($account, $this);
  }

}
