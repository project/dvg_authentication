<?php

/**
 * @file
 * Contains hooks and logic for DvG Authentication Age Check.
 */

use Drupal\dvg_authentication\AuthenticationManager;

define('DVG_AUTHENTICATION_AGE_CHECK_SESSION', 'dvg_authentication_age');

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * {@inheritdoc}
 */
function dvg_authentication_age_check_form_webform_configure_form_alter(array &$form, array &$form_state) {
  $node = $form['#node'];

  $authentication_manager = AuthenticationManager::getInstance();
  $settings = $authentication_manager->getNodeAuthenticationSettings($node);

  $form['dvg_authentication']['dvg_authentication_settings']['age_check'] = [
    '#type' => 'checkbox',
    '#title' => t('Minimum age check'),
    '#description' => t('Verify the age of the logged in user based on the date of birth supplied by the authentication method. When the date of birth is not available, an age verification step is added for the user to fill in.'),
    '#default_value' => $settings['age_check'] ?? 0,
    '#id' => 'dvg-authentication-age-check-enabled',
  ];
  $form['dvg_authentication']['dvg_authentication_settings']['minimum_age'] = [
    '#type' => 'textfield',
    '#attributes' => [
      ' type' => 'number',
      ' min' => 0,
    ],
    '#title' => t('Minimum age'),
    '#description' => t('The minimum age required to use this form'),
    '#maxlength' => 3,
    '#default_value' => $settings['minimum_age'] ?? 18,
    '#id' => 'dvg-authentication-minimum-age',
    '#states' => [
      'visible' => [
        '#dvg-authentication-age-check-enabled' => [
          'checked' => TRUE,
        ],
      ],
      'required' => [
        '#dvg-authentication-age-check-enabled' => [
          'checked' => TRUE,
        ],
      ],
    ],
  ];
  $form['dvg_authentication']['dvg_authentication_settings']['minimum_age_message'] = [
    '#type' => 'textarea',
    '#title' => t('Minimum age message'),
    '#description' => t('This message is displayed if the entered age is lower than the minimum age.'),
    '#default_value' => $settings['minimum_age_message'] ?? t('To complete this form you must meet the minimum age requirement.'),
    '#id' => 'dvg-authentication-age-check-failed-message',
    '#states' => [
      'visible' => [
        '#dvg-authentication-age-check-enabled' => [
          'checked' => TRUE,
        ],
      ],
      'required' => [
        '#dvg-authentication-age-check-enabled' => [
          'checked' => TRUE,
        ],
      ],
    ],
  ];

  array_unshift($form['#validate'], 'dvg_authentication_age_check_form_webform_configure_form_validate');
}

/**
 * Validation handler for webform_configure_form().
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function dvg_authentication_age_check_form_webform_configure_form_validate(array $form, array &$form_state) {
  if ($form_state['values']['dvg_authentication_enabled'] && $form_state['values']['dvg_authentication_settings']['age_check']) {
    if (empty($form_state['values']['dvg_authentication_settings']['minimum_age'])) {
      form_set_error('dvg_authentication_settings][minimum_age', t('!name field is required.', ['!name' => t('Minimum age')]));
    }
    if (empty($form_state['values']['dvg_authentication_settings']['minimum_age_message'])) {
      form_set_error('dvg_authentication_settings][minimum_age_message', t('!name field is required.', ['!name' => t('Minimum age message')]));
    }
  }
}

/**
 * Implements hook_node_view_alter().
 *
 * {@inheritdoc}
 */
function dvg_authentication_age_check_node_view_alter(array &$build) {
  $node = $build['#node'];

  if (!isset($build['webform'])) {
    // Bail out early if we're not on a webform.
    return;
  }

  // We use $build['webform']['#enabled'] to hide the form when external
  // login and optionally skip authentication is enabled. We can only set
  // #enabled to false if it's not FALSE already, because this value is set
  // by webform based on a couple of webform settings (e.g. open/closed,
  // max submissions per user and overall etc.).
  $authentication_manager = AuthenticationManager::getInstance();
  if (!$node->webform['status'] || !$authentication_manager->nodeHasAuthentication($node)) {
    // No external authentication required for this node.
    return;
  }

  // Load the authentication settings for this node.
  $authentication_settings = $authentication_manager->getNodeAuthenticationSettings($node);
  if (!empty($authentication_settings['age_check']) && $authentication_manager->isExternalUser()) {
    if (_dvg_authentication_age_check_get_age() === NULL) {
      // Try to get the users age based on date of birth.
      $dob = $authentication_manager->getExternalUser()
        ->getValue('date_of_birth');

      // No DoB from the authentication provider? Try to fetch the date of birth
      // from StUF prefill tokens if possible.
      if (empty($dob)) {
        $dob = token_replace('[dvg_personen:geboortedatum]');
      }

      // Still no date of birth? Show an extra form so the date of birth can be
      // entered manually. Hide the webform and show the login selection screen.
      if (empty($dob)) {
        $build['webform']['#enabled'] = FALSE;
        $build['external_authentication'] = drupal_get_form('dvg_authentication_age_date_of_birth_form');
        // Use the weight of the webform if possible to place the login options.
        $build['external_authentication']['#weight'] = $build['webform']['#weight'];
        return;
      }

      // Set the age.
      $date_of_birth = new DateTime();
      $date_of_birth->setTimestamp(strtotime($dob));
      _dvg_authentication_age_check_set_age($date_of_birth);
    }

    $age = _dvg_authentication_age_check_get_age();
    if ($age < $authentication_settings['minimum_age']) {
      // Block the form and show a message.
      drupal_set_message($authentication_settings['minimum_age_message'], 'warning');
      $build['webform']['#enabled'] = FALSE;
    }
  }
}

/**
 * Create the Date of birth input form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The date of birth form.
 */
function dvg_authentication_age_date_of_birth_form(array $form, array &$form_state) {
  $form['date_of_birth'] = [
    '#type' => 'date_popup',
    '#title' => t('Date of birth'),
    '#description' => t('Please enter your date of birth'),
    '#required' => TRUE,
    '#date_type' => DATE_ISO,
    '#date_format' => 'd-m-Y',
    '#date_increment' => 1,
    '#date_year_range' => '1900:0',
    '#date_label_position' => 'invisible',
  ];

  $form['buttons'] = [
    '#type' => 'container',
    '#prefix' => '<div class="form-actions">',
    '#suffix' => '</div>',
  ];
  $form['buttons']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Next'),
  ];

  return $form;
}

/**
 * Validates the Date of birth form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function dvg_authentication_age_date_of_birth_form_validate(array $form, array &$form_state) {
  if (!empty($form_state['values']['date_of_birth']) && !is_array($form_state['values']['date_of_birth'])) {
    $date_of_birth = new DateTime();
    $date_of_birth->setTimestamp(strtotime($form_state['values']['date_of_birth']));
    // Make sure the birth date isn't in the future.
    $today = new DateTime();
    if ($date_of_birth > $today) {
      form_set_error('date_of_birth', t('!field cannot be in the future.', ['!field' => $form['date_of_birth']['#title']]));
    }
  }
}

/**
 * Submit handler for the Date of birth input form.
 *
 * @param array $form
 *   The form.
 * @param array $form_state
 *   The form state.
 */
function dvg_authentication_age_date_of_birth_form_submit(array $form, array &$form_state) {
  if (!empty($form_state['values']['date_of_birth'])) {
    $date_of_birth = new DateTime();
    $date_of_birth->setTimestamp(strtotime($form_state['values']['date_of_birth']));
    // Set the date of birth.
    _dvg_authentication_age_check_set_age($date_of_birth);
  }
}

/**
 * Implements hook_user_logout().
 */
function dvg_authentication_age_check_user_logout($account) {
  // Clear the age for the next user.
  unset($_SESSION[DVG_AUTHENTICATION_AGE_CHECK_SESSION]);
}

/**
 * Helper function to calculate an age from a Unix timestamp.
 *
 * @param \DateTime $date_of_birth
 *   Date of birth to set the age for.
 */
function _dvg_authentication_age_check_set_age(DateTime $date_of_birth) {
  $today = new DateTime();
  $_SESSION[DVG_AUTHENTICATION_AGE_CHECK_SESSION] = $today->diff($date_of_birth)->y;
}

/**
 * Helper function to get the age.
 *
 * @return int|null
 *   An integer with the age of the user or NULL if not set.
 */
function _dvg_authentication_age_check_get_age() {
  return $_SESSION[DVG_AUTHENTICATION_AGE_CHECK_SESSION] ?? NULL;
}
