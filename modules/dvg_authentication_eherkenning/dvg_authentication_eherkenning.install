<?php

/**
 * @file
 * Install and update functions for DvG Authentication eHerkenning.
 */

/**
 * Implements hook_install().
 *
 * Migrate the eherkenning webform settings for webforms with eherkenning
 * enabled.
 *
 * {@inheritdoc}
 */
function dvg_authentication_eherkenning_install() {
  // Get all roles.
  $roles = user_roles();
  $eherkenning_rids = [
    'level_1' => array_search('eherkenning_eh1', $roles),
    'level_2' => array_search('eherkenning_eh2', $roles),
    'level_2plus' => array_search('eherkenning_eh2plus', $roles),
    'level_3' => array_search('eherkenning_eh3', $roles),
  ];
  // Remove non-existing roles from array.
  $eherkenning_rids = array_filter($eherkenning_rids, function ($rid) {
    return $rid !== FALSE;
  });
  $ext_user_rid = array_search('external user', $roles);

  // Is there anything to migrate?
  if (empty($eherkenning_rids) || $ext_user_rid === FALSE) {
    return;
  }
  // Get all webforms.
  $nodes = node_load_multiple([], ['type' => 'webform']);

  foreach ($nodes as $node) {
    // Get webform roles.
    $role_ids = $node->webform['roles'];
    $enabled_roles = array_intersect($eherkenning_rids, $role_ids);
    if (!empty($enabled_roles)) {
      // Remove old eherkenning setting.
      foreach ($node->webform['roles'] as $key => $rid) {
        if (in_array($rid, $eherkenning_rids)) {
          unset($node->webform['roles'][$key]);
        }
      }

      // Add eherkenning to the new settings.
      if (!in_array($ext_user_rid, $role_ids)) {
        // Add external user role only once.
        $node->webform['roles'][] = $ext_user_rid;
      }
      // Set new eherkenning property.
      $settings = [];
      if (!empty($node->webform['dvg_authentication_settings'])) {
        $settings = unserialize($node->webform['dvg_authentication_settings'], ['allowed_classes' => FALSE]);
      }
      $settings['methods']['eherkenning']['enabled'] = 1;
      // Set lowest enabled level.
      $enabled_levels = array_keys($enabled_roles);
      $settings['methods']['eherkenning']['level'] = reset($enabled_levels);
      $node->webform['dvg_authentication_settings'] = serialize($settings);
      // Save webform.
      node_save($node);
    }
  }
}
