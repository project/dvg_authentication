<?php

namespace Drupal\dvg_authentication_eherkenning;

use Drupal\dvg_authentication\DvgAuthenticationException;
use Drupal\dvg_authentication\SamlAuthenticationProviderBase;
use Drupal\dvg_authentication\SamlUser;

/**
 * Class EherkenningAuthenticationProvider.
 */
class EherkenningAuthenticationProvider extends SamlAuthenticationProviderBase {

  /**
   * Authentication levels.
   */
  const LEVEL_1 = 'level_1';
  const LEVEL_2 = 'level_2';
  const LEVEL_2PLUS = 'level_2plus';
  const LEVEL_3 = 'level_3';

  /**
   * List of supported eherkenning versions.
   *
   * @var array
   */
  protected static $versions = ['1.7', '1.9', '1.11'];

  /**
   * List of namespaces per version keyed by the eHerkenning version.
   *
   * @var array
   */
  protected static $xmlNamespaces = [
    '1.7' => 'urn:nl:eherkenning:',
    '1.9' => 'urn:etoegang:',
    '1.11' => 'urn:etoegang:',
  ];

  /**
   * {@inheritdoc}
   *
   * @var array
   */
  protected $samlAttributeMapping = [
    'ServiceID' => 'identifier',
    'KvKnr' => 'kvk_number',
    'Vestigingsnr' => 'kvk_department_number',
  ];

  /**
   * {@inheritdoc}
   */
  protected $isLogoutReturnToCallbackSupported = FALSE;

  /**
   * EHerkenning requires an extra logo per authentication level.
   *
   * @var bool
   */
  protected $useLevelLogos = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return 'eherkenning';
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return t('eHerkenning');
  }

  /**
   * {@inheritdoc}
   */
  public function getButtonDescription() {
    $args = ['@url' => 'https://www.eherkenning.nl'];
    return t('You are an entrepreneur and registered with the Chamber of Commerce. Login with eHerkenning. More information can be found on <a href="@url">eherkenning.nl</a>.', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getLevels() {
    return [
      static::LEVEL_1 => t('level 1'),
      static::LEVEL_2 => t('level 2'),
      static::LEVEL_2PLUS => t('level 2+'),
      static::LEVEL_3 => t('level 3'),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @see https://afsprakenstelsel.etoegang.nl/display/as/Interface+specifications+DV-HM
   */
  protected function getMinimumAuthnContextClassRef($level) {
    switch ($level) {
      case static::LEVEL_1:
        return 'urn:etoegang:core:assurance-class:loa1';

      case static::LEVEL_2:
        return 'urn:etoegang:core:assurance-class:loa2';

      case static::LEVEL_2PLUS:
        return 'urn:etoegang:core:assurance-class:loa2plus';

      case static::LEVEL_3:
        return 'urn:etoegang:core:assurance-class:loa3';

      default:
        throw new DvgAuthenticationException('Invalid authentication level');

    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultConfiguration() {
    return [
      'show_confirmation_page' => FALSE,
      'version' => '1.11',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm() {
    $form = parent::buildConfigurationForm();
    $form['version'] = [
      '#type' => 'select',
      '#title' => t('eHerkenning version'),
      '#options' => drupal_map_assoc(static::$versions),
      '#default_value' => $this->getConfig('version'),
      '#required' => TRUE,
      '#weight' => -2,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getUser(\stdClass $account) {
    return new SamlUser($account, $this);
  }

}
